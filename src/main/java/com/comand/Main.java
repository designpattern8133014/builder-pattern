package com.comand;

public class Main {

    public static void main(String[] args) {
        Employee employee = new Employee();
        employee.setEmail("a@bGmail.com");
        // ...
        // ...

        /*builder pattern*/
        Employee.EmployeeBuilder builder = new Employee.EmployeeBuilder();
        Employee result = builder.firstName("Taraneh").lastName("Ranjbar").email("taraneh.ranjbar@gmail.com")
                .education("MS").mobile("0912").birthDate("1992").fatherName("ali")
                .nationId("14714765").residentAddress("tehran").workAddress("CAN").phone("").build();

        System.out.println(result.toString());
    }
}
