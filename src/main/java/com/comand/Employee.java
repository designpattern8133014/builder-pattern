package com.comand;

public class Employee {

    private String firstName;
    private String lastName;
    private String fatherName;
    private String workAddress;
    private String nationId;
    private String phone;
    private String mobile;
    private String email;
    private String education;
    private String residentAddress;
    private String birthDate;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String fatherName, String workAddress, String nationId, String phone, String mobile, String email, String education, String residentAddress, String birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.workAddress = workAddress;
        this.nationId = nationId;
        this.phone = phone;
        this.mobile = mobile;
        this.email = email;
        this.education = education;
        this.residentAddress = residentAddress;
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getNationId() {
        return nationId;
    }

    public void setNationId(String nationId) {
        this.nationId = nationId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getResidentAddress() {
        return residentAddress;
    }

    public void setResidentAddress(String residentAddress) {
        this.residentAddress = residentAddress;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", nationId='" + nationId + '\'' +
                ", phone='" + phone + '\'' +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", education='" + education + '\'' +
                ", residentAddress='" + residentAddress + '\'' +
                ", birthDate='" + birthDate + '\'' +
                '}';
    }

    public static class EmployeeBuilder { //nested class to do builder pattern

        private String firstName;
        private String lastName;
        private String fatherName;
        private String workAddress;
        private String nationId;
        private String phone;
        private String mobile;
        private String email;
        private String education;
        private String residentAddress;
        private String birthDate;

        public EmployeeBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public EmployeeBuilder fatherName(String fatherName) {
            this.fatherName = fatherName;
            return this;
        }

        public EmployeeBuilder workAddress(String workAddress) {
            this.workAddress = workAddress;
            return this;
        }

        public EmployeeBuilder nationId(String nationId) {
            this.nationId = nationId;
            return this;
        }

        public EmployeeBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public EmployeeBuilder mobile(String mobile) {
            this.mobile = mobile;
            return this;
        }

        public EmployeeBuilder email(String email) {
            this.email = email;
            return this;
        }

        public EmployeeBuilder education(String education) {
            this.education = education;
            return this;
        }

        public EmployeeBuilder residentAddress(String residentAddress) {
            this.residentAddress = residentAddress;
            return this;
        }

        public EmployeeBuilder birthDate(String birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Employee build(){
            Employee employee = new Employee();
            employee.setFirstName(firstName);
            employee.setLastName(lastName);
            employee.setEmail(email);
            employee.setFatherName(fatherName);
            employee.setEducation(education);
            employee.setMobile(mobile);
            employee.setBirthDate(birthDate);
            employee.setResidentAddress(residentAddress);
            employee.setWorkAddress(workAddress);
            employee.setNationId(nationId);
            employee.setPhone(phone);
            return employee;
        }
}
}
